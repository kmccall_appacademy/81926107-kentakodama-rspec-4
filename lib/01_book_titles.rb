class Book

    attr_accessor :book, :title
    # multiple accessors
    def title
      words = @title.split(" ")
      words.first.capitalize!
      exceptions = ["the", "a", "an", "and", "but", "or", "in", "to", "of"]
      words.map! do |word|
            if exceptions.include? word
                word
            else
                word.capitalize
            end
      end
      words.join(" ")
    end
end
