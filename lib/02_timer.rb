class Timer

  attr_accessor :seconds

  def initialize(input=0)
    @seconds = input
  end

  def time_string
    hours = @seconds/3600
    @seconds = @seconds % 3600
    minutes = @seconds/60
    @seconds = @seconds % 60
    seconds = @seconds

    "#{pad(hours)}:#{pad(minutes)}:#{pad(seconds)}"

  end


  private

  def pad(num)
    if num < 10
      return "0#{num}"
    else
      return num.to_s
    end
  end

end

test1 = Timer.new
p test1.seconds = 12
p test1.time_string
