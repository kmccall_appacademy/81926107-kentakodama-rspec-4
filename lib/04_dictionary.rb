class Dictionary

  attr_accessor :entries, :sort

  def initialize
    @entries = Hash.new
  end

  def add(input)
    @entries[input] = nil if input.is_a? String
    @entries.merge!(input) if input.is_a? Hash #strange way of inputing k, v pairs
  end

  def include?(word)
    @entries.include?(word)
  end

  def find(str)
    return {} if @entries.empty?
    @entries.select{|k, v| k.include?(str)}
  end

  def sort
    @entries.sort_by{|k, v| k}.to_h
  end

  def keywords
    sorted = sort
    sorted.keys
  end

  def printable
    #  "[apple] \"fruit\"\n[fish] \"aquatic animal\"\n[zebra] \"African land animal with stripes\"
    sorted = sort
    string = ""
    sorted.each do |key, value| 
        string += "[#{key}] \"#{value}\"\n"
    end
    string.chomp
  end

end

example = Dictionary.new
example.add('fish' => 'aquatic animal')
example.add('bear')
p example.printable
