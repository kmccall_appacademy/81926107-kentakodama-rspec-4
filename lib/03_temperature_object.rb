class Temperature
  # TODO: your code goes here!

    def initialize(pair)
        @temp = pair #store key value in @temp hash
    end

    def in_celsius
        if @temp.include? :c
            @temp[:c]
        else
            (@temp[:f] - 32) * (5.0/9.0)
        end
    end

    def in_fahrenheit
        if @temp.include? :f
            @temp[:f]
        else
            (@temp[:c] * (9.0/5.0)) + 32
        end
    end

    def self.from_fahrenheit(degrees)
        Temperature.new(:f => degrees)
        # Temperature.new(f: degrees)
    end

    def self.from_celsius(degrees)
        Temperature.new(:c => degrees)
    end


end

class Celsius < Temperature
    def initialize(degrees)
        @temp = { c: degrees}
    end

end

class Fahrenheit < Temperature
    def initialize(degrees)
        @temp = {f: degrees}
    end
end
